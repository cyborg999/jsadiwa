<?php
namespace app\view;

class homepage extends \rueckgrat\mvc\FastView {
    protected $user;
    protected $post;
    
    public function __construct() {
        parent::__construct();
        
        $this->cssFiles[]       = 'public/css/bootstrap.min.css';
        $this->cacheDisabled    = TRUE;
        $this->title            = 'Welcome to my awesome page';
    }
    
    public function renderHomepage(){
        $this->pageContent = $this->getCompiledTpl("homepage/index");

        return $this->renderMainPage();
    }
    
    public function renderWallPage($post){
        $this->cssFiles[]   = 'public/css/blog.css';
        $this->title        = "Wall";
        $this->pageContent  = $this->getCompiledTpl("wall/wall");
        $this->post = $post;
        
        return $this->renderMainPage();
    }
    
    public function renderProfilePage(\app\mapper\User $user){
        $this->cssFiles[]   = 'public/css/blog.css';
        $this->title        = ucfirst($user->getUsername())."'s Wall";
        $this->user         = $user;
        $this->pageContent  = $this->getCompiledTpl("wall/profile");
        
        return $this->renderMainPage();
    }
    
}
