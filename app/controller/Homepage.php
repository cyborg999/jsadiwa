<?php
namespace app\controller;

use rueckgrat\mvc\DefaultController;
use rueckgrat\security\Input;

class Homepage extends DefaultController {
    protected $view;
    protected $userModel;
    
    public function __construct() {
        parent::__construct();
        
        $this->view = new \app\view\homepage();
        $this->userModel = new \app\model\UserModel();
    }
    
    public function index(){
        return $this->view->renderHomepage();
    }
    
    public function loginUser(){
        $user = new \app\mapper\User();
        $user->map(array(
            'username' => Input::p('username'),
            'password' => md5(Input::p('password'))
        ));
        
        $userInfo   = $this->userModel->findUser($user);
        $userExists = $userInfo->rowCount();
        
        if($userExists){
           $_SESSION['user_info'] = $this->userModel->getUserProfile($userInfo);
            
            header('Location:?c=homepage&m=profile');
        } else {
            $_SESSION['invalid_login'] = TRUE;
            header('Location:?c=homepage&m=index');
        }
    }
    
    public function logout(){
        if(isset($_SESSION['user_info'])){
            session_destroy();
        } 

        header('Location:?c=homepage&m=index');
    }
    
    public function profile(){
        if(! isset($_SESSION['user_info'])){
            $this->logout();
        } else {
            $id = $_SESSION['user_info']['id'];
            $user = $this->userModel->getById($id);

            return $this->view->renderProfilePage($user);
        }
    }
    
    public function updateUser(){
        $user = new \app\mapper\User();
        $user->map(array(
            'id'        => Input::p('id'),
            'firstname' => Input::p('firstname'),
            'lastname'  => Input::p('lastname'),
            'age'       => Input::p('age'),
            'username' => Input::p('username'),
            'password' => $this->userModel->getUserPassword(Input::p('id'))
        ));
        
        $this->userModel->updateUser($user);
        
        header('Location:?c=homepage&m=profile');
    }
    
    public function registerUser(){
        $user = new \app\mapper\User();
        $user->map(array(
            'firstname'     => '',
            'lastname'      => '',
            'age'       => 0,
            'username'  => Input::p("username"),
            'password'  => md5(Input::p('password'))
        ));
        
        $this->userModel->registerUser($user);
        
        header('Location:?c=homepage&m=index');
    }
}
