<?php
namespace app\controller;

use rueckgrat\mvc\DefaultController;
use rueckgrat\security\Input;

class Wall extends DefaultController {
    protected $view; 
    protected $wallModel;
    
    public function __construct(){
        parent::__construct();
        
        $this->view = new \app\view\homepage();
        $this->wallModel = new \app\model\postModel();
    }
    
    public function index(){
        $post = $this->wallModel->getAllPosts();
        
        return $this->view->renderWallPage($post);
    }
    
    public function delete(){
        $postId = new \app\mapper\post();
        $postId = $this->wallModel->getById(Input::g('id'));
        
        $this->wallModel->deletePost($postId);
        
        header('Location:?c=wall&m=index');
    }
}
