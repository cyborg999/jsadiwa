<?php
namespace app\model;

use rueckgrat\mvc\DefaultDBModel;

class UserModel extends DefaultDBModel {
    public function __construct() {
        parent::__construct('user');
    }
    
    public function findUser(\app\mapper\User $user){
       $sql = 'SELECT id FROM user '
                . 'WHERE username = "'.$user->getUsername().'" '
                . 'AND password = "'.$user->getPassword().'"'
               . 'LIMIT 1';
        
       $user = $this->db->query($sql);
        
        return $user;
    }
    
    public function getById($id){
        $user   = new \app\mapper\User();
        $row    = $this->get($id);
        
        $user->map($row);
        
        return $user;
    }
    
    public function getUserProfile($user){
        while($row = $user->fetch()){
            return $row; // sql query uses limit 1
        }
    }
    
    public function registerUser(\app\mapper\User $user){
//        todo:add validation
        $this->create($user);
        $_SESSION['registration_success'] = TRUE;
    }
    
    public function updateUser(\app\mapper\User $user){
//        todo: add validation
          $this->save($user);
          $_SESSION['update_success'] = TRUE;
    }
    
    public function getUserPassword($id){
        $user = $this->getById($id);
        
        return $user->getPassword();
    }
}
