<?php
namespace app\model;

use rueckgrat\mvc\DefaultDBModel;

class postModel extends DefaultDBModel {
    public function __construct() {
        parent::__construct("post");
    }
    
    public function getAllPosts(){
        $posts = array();

        $sql = "SELECT * FROM post ORDER BY date DESC LIMIT 5";
        $record = $this->db->query($sql);
        
        while($row = $record->fetch()){
            $post = new \app\mapper\post();
            $post->map($row);
            
            $posts[] = $post;
        }
        
        return $posts;
    }
    
    public function getByid($id){
        $post = new \app\mapper\post();
        $row = $this->get($id);
        $post->map($row);
        
        return $post;
    }
    
    public function deletePost(\app\mapper\post $post){
        $this->delete($post);
    }
}
