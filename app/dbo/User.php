<?php
namespace app\dbo;

class User extends \rueckgrat\db\Mapper {
    protected $firstname;
    protected $lastname;
    protected $age;
    protected $username;
    protected $password;

    public function __construct() {
        parent::__construct();
    }
    
    public function getFirstname(){
        return $this->firstname;
    }
    
    public function getLastname(){
        return $this->lastname;
    }
    
    public function getAge(){
        return $this->age;
    }
    
    public function getUsername(){
        return $this->username;
    }
    
    public function getPassword(){
        return $this->password;
    }
}
