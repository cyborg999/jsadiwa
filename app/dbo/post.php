<?php
namespace app\dbo;

class post extends \rueckgrat\db\Mapper {
    protected $title;
    protected $desc;
    protected $user_id;
    protected $date;
    
    public function __construct(){
        parent::__construct();
    }
    
    public function getTitle(){
        return $this->title;
    }
    
    public function getDesc(){
        return $this->desc;
    }
    
    public function getUserId(){
        return $this->user_id;
    }
    
    public function getDate(){
        return date('F/j/Y',strtotime($this->date));
    }
}
