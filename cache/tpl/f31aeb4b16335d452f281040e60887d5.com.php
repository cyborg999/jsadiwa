<!DOCTYPE html>
<html>
    <head>
        <title><?= $this->title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="public/css/style.css">
        <link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">forum test</a>
    </div>
    <?php if(isset($_SESSION['invalid_login']) && $_SESSION['invalid_login']) { ?>
        <a href="#" class="alert-link pull-right">Invalid login info</a>
    <?php } unset($_SESSION['invalid_login']); ?> 
    <div id="navbar" class="navbar-collapse collapse">
      <form class="navbar-form navbar-right" role="form" action="index.php" method="post">
        <input type="hidden" name="c" value="homepage"/>
        <input type="hidden" name="m" value="loginUser"/>
        <div class="form-group">
          <input type="text" placeholder="Email" name="username" class="form-control">
        </div>
        <div class="form-group">
          <input type="password" placeholder="Password" name="password" class="form-control">
        </div>
        <button type="submit" class="btn btn-success">Sign in</button>
      </form>
    </div>
  </div>
</nav>
<div class="jumbotron">
  <div class="container">
    <?php if(isset($_SESSION['registration_success']) && $_SESSION['registration_success']) { ?>
        <div class="alert alert-success" role="alert">
            <a href="#" class="alert-link">Registration Success. You can now login</a>
        </div>
    <?php } unset($_SESSION['registration_success']); ?> 
    <h1>Create an account</h1>
    <form class="form-horizontal" role="form" action="index.php" method="post">
        <input type="hidden" name="c" value="homepage"/>
        <input type="hidden" name="m" value="registerUser"/>
        <div class="form-group">
            <label for="username" class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
              <input type="text" name="username" class="form-control" id="username" placeholder="username..."/>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
              <input type="password" name="password" class="form-control" id="password" placeholder="password..."/>
            </div>
        </div>
        <div class="form-group">
            <!--<label for="password2" class="col-sm-2 control-label">Retype Password</label>-->
            <div class="col-sm-10">
              <!--<input type="password" class="form-control" id="password2" placeholder="password..."/>-->
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success active">Register</button>
            </div>
        </div>
    </form>
  </div>
</div>

        </div>
        <?php
        $cb = \rueckgrat\xhr\CallbackManager::getCallbacks();
        if (count($cb) > 0) {
            echo '<script type="text/javascript"">$(document).ready(function(){handleCallbacks(' . json_encode(array('callbacks' => $cb)) . ');});</script>' . "\n";
        }
        ?>
                <script type="text/javascript" src="public/js/main.js"></script>
    </body>
</html>