<div class="blog-masthead">
      <div class="container blog-masthead">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="?c=wall&m=index">Home</a>
          <a class="blog-nav-item" href="?c=homepage&m=profile"">My Profile</a>
          <a class="blog-nav-item pull-right" href="?c=homepage&m=logout"">logout</a>
        </nav>
      </div>
    </div>

    <div class="container">

      <div class="blog-header">
      </div>

      <div class="row">

        <div class="col-sm-8 blog-main">
            <?php foreach($this->post as $post){ ?>
                <div class="blog-post">
                    <h2 class="blog-post-title"><?php echo $post->getTitle(); ?></h2>
                    <p class="blog-post-meta"><?php echo $post->getDate(); ?>  - <a href="?c=wall&m=delete&id=<?php echo $post->getId(); ?>">Delete</a></p>

                    <blockquote>
                      <p><?php echo $post->getDesc(); ?></p>
                    </blockquote>
                    <hr>
                </div><!-- /.blog-post -->
            <?php } ?>
          
        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
          </div>
          <div class="sidebar-module">
            <h4>Archives</h4>
            <ol class="list-unstyled">
              <li><a href="#">December 2014</a></li>
              <li><a href="#">November 2014</a></li>
            </ol>
          </div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->