<div class="blog-masthead">
      <div class="container blog-masthead">
        <nav class="blog-nav">
            <a class="blog-nav-item" href="?c=wall&m=index">Home</a>
            <a class="blog-nav-item active" href="?c=homepage&m=profile"">My Profile</a>
            <a class="blog-nav-item pull-right" href="?c=homepage&m=logout"">logout</a>
        </nav>
      </div>
    </div>

    <div class="container">

      <div class="blog-header">
      </div>

      <div class="row">
          <div class="columns col-lg-12">
            <form class="form-horizontal" role="form" action="index.php" method="post">
                <input type="hidden" name="c" value="homepage"/>
                <input type="hidden" name="m" value="updateUser"/>
                <input type="hidden" name="id" value="<?php echo $this->user->getId();?>"/>
                <!--<input type="hidden" name="id" value="<?php echo $this->user->getPassword();?>"/>-->
                <div class="form-group">
                    <label for="username" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                      <input type="text" name="username" class="form-control" id="username" value="<?php echo $this->user->getUsername();?>" placeholder="username..."/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstname" class="col-sm-2 control-label">Firstname</label>
                    <div class="col-sm-10">
                      <input type="text" name="firstname" class="form-control" id="firstname" value="<?php echo $this->user->getFirstname();?>" placeholder="firstname..."/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastname" class="col-sm-2 control-label">Lastname</label>
                    <div class="col-sm-10">
                      <input type="text" name="lastname" class="form-control" id="lastname" value="<?php echo $this->user->getLastname();?>" placeholder="lastname..."/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="age" class="col-sm-2 control-label">Age</label>
                    <div class="col-sm-10">
                      <input type="text" name="age" class="form-control" id="age" value="<?php echo $this->user->getAge();?>" placeholder="age..."/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success active">Update</button>
                    </div>
                </div>
            </form>
            <?php if(isset($_SESSION['update_success']) && $_SESSION['update_success']) { ?>
              <div class="alert alert-success" role="alert">
                  <a href="#" class="alert-link">Record is successfully updated. </a>
              </div>
            <?php } unset($_SESSION['update_success']); ?> 
          </div>
      </div><!-- /.row -->

    </div><!-- /.container -->